import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { User } from 'src/app/entity/user';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<User>;

  users: User[];
  displayedColumns = ['ID', 'Email', 'EmailVerified'];
  loading: boolean;
  dataSource: MatTableDataSource<any>; 

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.loading = true;
    this.authService.getUsers().subscribe(users => {
      this.dataSource = new MatTableDataSource(users); 
      this.dataSource.sort = this.sort;
      //console.log(users);
      // this.users = users;
      setTimeout(() => {
        this.loading = false;
      }, 500);
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  trackByUid(index, item) {
    return item.uid;
  }

}
