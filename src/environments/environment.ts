// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDTOndkFjA7wGoeSh8KubSGRCCcBLf2aew",
    authDomain: "auth-5b019.firebaseapp.com",
    databaseURL: "https://auth-5b019.firebaseio.com",
    projectId: "auth-5b019",
    storageBucket: "auth-5b019.appspot.com",
    messagingSenderId: "110812291132",
    appId: "1:110812291132:web:3f3ebeb61d22472e83a339"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
